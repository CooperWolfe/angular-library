import { Component, Input } from '@angular/core'

@Component({
  selector: 'wwd-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.styl']
})
export class HeaderComponent {

  @Input() imageUrl: string
  @Input() text: string

}
