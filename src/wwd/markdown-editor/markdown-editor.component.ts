import { Component, ElementRef, forwardRef, Input, ViewChild } from '@angular/core';

import { Converter } from 'showdown'
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'wwd-markdown-editor',
  templateUrl: './markdown-editor.component.html',
  styleUrls: ['./markdown-editor.component.styl'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MarkdownEditorComponent),
      multi: true
    }
  ]
})
export class MarkdownEditorComponent implements ControlValueAccessor {

  converter = new Converter()

  @Input() _markdown = ''
  get markdown(): string {
    return this._markdown
  }
  set markdown(md: string) {
    this._markdown = md
    this.propogateChange(this._markdown)
  }

  propogateChange: (_: any) => {}
  registerOnChange(fn: any): void {
    this.propogateChange = fn
  }
  registerOnTouched(fn: any): void {}
  setDisabledState(isDisabled: boolean): void {}
  writeValue(obj: any): void {
    if (obj !== undefined && typeof obj === 'string') this.markdown = obj
  }

  @ViewChild('textarea') textarea: ElementRef
  @ViewChild('preview') preview: ElementRef

  bold() { this.insertAtCaret('**Text**', { start: 2, end: 6 }) }
  italic() { this.insertAtCaret('*Text*', { start: 1, end: 5 }) }
  link() { this.insertAtCaret('[Text](Url)', { start: 1, end: 5 }) }
  quote() { this.insertAtCaret('\n\n> Text', { start: 4, end: 8 }) }
  code() { this.insertAtCaret('`Text`', { start: 1, end: 5 }) }
  image() { this.insertAtCaret('![Title](Url)', { start: 2, end: 7 }) }
  ol() { this.insertAtCaret('\n\n 1. Text', { start: 6, end: 10 }) }
  ul() { this.insertAtCaret('\n\n - Text', { start: 5, end: 9 }) }
  header() { this.insertAtCaret('\n# Text', { start: 3, end: 7 }) }
  rule() { this.insertAtCaret('\n\n----------\n') }

  html(md: string): string {
    return this.converter.makeHtml(md)
  }

  private insertAtCaret(text: string, selection?: {start: number, end: number}) {
    const el = this.textarea.nativeElement
    const trimmed = text.trim()

    if (trimmed !== text && el.value === '') {
      const dif = text.length - trimmed.length
      selection.start -= dif
      selection.end -= dif
      text = trimmed
    }

    el.focus()

    // IE
    // noinspection TypeScriptUnresolvedVariable
    const webSelection = (<any>document).selection
    if (webSelection) {
      const range = webSelection.createRange()
      range.text = text
    }
    // Other
    else if (el.selectionStart || el.selectionStart === '0') {
      const startPos = el.selectionStart
      const endPos = el.selectionEnd
      const scrollTop = el.scrollTop
      el.value = el.value.substring(0, startPos) + text + el.value.substring(endPos, el.value.length)
      el.selectionStart = startPos + (selection.start || text.length)
      el.selectionEnd = startPos + (selection.end || text.length)
      el.scrollTop = scrollTop
    }
    // No selection
    else {
      el.value += text
      el.selectionStart = selection.start || text.length
      el.selectionEnd = selection.end || text.length
    }

    el.focus()
  }
}
