import { BrowserModule } from '@angular/platform-browser'
import { NgModule } from '@angular/core'

import { TestComponent } from './test.component'
import { MarkdownEditorComponent } from './markdown-editor/markdown-editor.component'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms';
import { HeaderComponent } from './header/header.component'


@NgModule({
  declarations: [
    TestComponent,
    MarkdownEditorComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    BrowserModule
  ],
  providers: [],
  bootstrap: [TestComponent],
  exports: [
    MarkdownEditorComponent,
    HeaderComponent
  ]
})
export class WwdModule { }
